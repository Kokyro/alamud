# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
# ==============================================================================

from .event import Event1


class QuestEvent(Event1):
    NAME = "quest"

    def perform(self):
        debut = ["Aller récupérer le gel hydroalcoolique, que l'on avait laisser dans la salle de bain"]
        q1 = ["Prendre une attestation", "Prendre son portefeuille", "Prendre ses clés de voiture"]
        q2 = ["Acheter du gel hydroalcoolique"]
        q3 = ["Rentrer se laver les mains"]
        objet = ['gel-hydroalcoolique-001', 'attestation-000', 'portefeuille-000', 'cles-000',
                 'gel-hydroalcoolique-000']
        quetes = debut
        info = "q"
        avance = []
        self.buffer_clear()
        it = self.actor.contents()
        if not self.actor.is_empty():
            while True:
                try:
                    ide = next(it).id
                except StopIteration:
                    break
                if ide in objet:
                    avance.append(objet.index(ide))
            if 0 in avance:
                quetes = q1
                info = "q1"
                if 1 in avance and 2 in avance and 3 in avance:
                    quetes = q2
                    info = "q2"
                    if 4 in avance:
                        quetes = q3
                        info = "q3"
            self.buffer_inform("quest."+info)
        else:
            self.buffer_inform("quest."+info)
        self.buffer_inform("quest.intro")
        self.buffer_append("<ul>")
        for x in quetes:
            self.buffer_append("<li>")
            self.buffer_append(x)
            self.buffer_append("</li>")
        self.buffer_append("</ul>")
        self.actor.send_result(self.buffer_get())
